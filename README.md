# new_domain


This role will create a brand new Primary Domain Controller with a Active Directory Domain/Forest.  No hardening is applied.

Works on

- Windows Server 2019
- Windows Server 2016
- Windows Server 2012R2

## Requirements

- `python3-winrm` (`pywinrm`) is needed for WinRM.

## Role Variables

### `defaults/main.yml`

| Variable                         | Default value                       | Explanation |
|:---------------------------------|:------------------------------------|:------------|
| new_domain_domainadmin_username      | Administrator                       | Domain admin account used to add a new DC to an existing Domain  |
| new_domainadmin_password         | P@ssw0rd!                           | The password of the domain admin account. Recommend using ansible vault to obscure this |
| new_domain_localadmin_username       | Administrator                       | Local administrator account, generally the Built-in Administrator account   |
| new_domain_localadmin_password          | P@ssw0rd!                           | The password of Built-in Administrator account. This password (if new_domain_localadmin_username left to the default value) will become the password of NETBIOS\Administrator. Change this to a strong password. Recommend using ansible vault to obscure this |
| new_domain_dns_ip1                      | "{{ new_domain_static_ip1  }}"                     | This will set the first interface to use the desired DNS IP, defaults to the desired static IP address |
| new_domain_dns_ip2                       | 192.168.1.10                       | When defined, this will set the first interface to use the desired secondary DNS IP (not required) |
| new_domain_dns2_ip1                      | 192.165.1.11                       | When defined, this will set the second interface to use the desired DNS IP, only required if using multiple interfaces |
| new_domain_dns2_ip2                      | 192.165.1.10                        | When defined, this will set the second interface to use the desired DNS IP, only used if using multiple interfaces |
| new_domain_static_ip1                    | 192.168.1.9                       | Set to desired static IP for first interface |
| new_domain_static_ip2                    | 192.165.1.9                       | Set to desired static IP for second interface (not required)|
| new_domain_gw_int1                       | 192.168.1.254                      | IP of the first interface's gateway |
| new_domain_gw_int2                       | 192.165.1.254                      | IP of the second interface's gateway |
| new_domain_newhostname                   | dc1                                | New hostname of the DC |
| new_domain_domain                       | ad.example.test                     | The Domain of the new Active Directory Forest |
| new_domain_netbios                      | TEST                                | The NetBIOS of the new Active Directory Forest. Change this depending on your needs. |
| new_domain_domain_safe_mode_password    | P@ssw0rd!                           | The Domain Safe Mode password. Change this to a strong password. Recommend using ansible vault to obscure this |
| new_domain_domain_functional_level      | Default | Specifies the domain functional level of the first domain in the creation of a new forest. The domain functional level cannot be lower than the forest functional level, but it can be higher. Change this depending on your needs. |
| new_domain_forest_functional_level      | Default | Specifies the forest functional level for the new forest. The default forest functional level in Windows Server is typically the same as the version you are running. Change this depending on your needs. |
| new_domain_required_psmodules           | [xPSDesiredStateConfiguration, NetworkingDsc, ComputerManagementDsc, ActiveDirectoryDsc]              | PowerShell/DSC modules to install from the files/DscResources folder. Always make sure to include `ActiveDirectoryDsc`for the `WaitForAD`-check. 9/10 times you should leave this to the default value. |
| new_domain_required_features            | ["AD-domain-services", "DNS","DHCP","NPAS"]       | Windows Features that should be installed on the Domain Controller. Defaults to AD-domain-services and DNS. 9/10 times you should leave this to the default value. |
| new_domain_src                           | /etc/ansible/roles/ansible_role_new_domain/files    | Location of the roles' files.  Change if you put the role elsewhere |
| new_domain_dns_nics                     | *                                   | The name of the ethernet adapter to setup DNS on. Defaults to wildcard, leave as such. |

## Dependencies

- 

## Example Playbook

    - hosts: primarydomaincontroller
      roles:
         - new_domain

## Local Development

This role includes a Vagrantfile that will spin up a local Windows Server 2019 VM in Virtualbox.  
After creating the VM it will automatically run our role.

### Development requirements

`pip3 install pywinrm`

#### Usage

- Run `vagrant up` to create a VM and run our role.
- Run `vagrant provision` to reapply our role.
- Run `vagrant destroy -f && vagrant up` to recreate the VM and run our role.
- Run `vagrant destroy` to remove the VM.

## License

MIT

## Authors

- John Potter, DREAM Team.  Developed from the work of Justin Perdok ([@justin-p](https://github.com/justin-p/))




